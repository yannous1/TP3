// Port to listen requests from
var port = 1234;

// Modules to be used
var shajs = require('sha.js');
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var cookieParser = require('cookie-parser');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

app.use(cookieParser());

var rand = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

var alea = function() {
    return rand() + rand(); // to make it longer
};


// Serve static filestr
app.post("/deco", function(req, res, next) {
    var vide = " ";
    db.all('UPDATE sessions SET token=? WHERE token=?;', [vide ,req.cookies['token']], function (err, data) {
        res.clearCookie('token');
        res.sendFile(__dirname + '/public/index.html');
    });
});
app.get("/", function(req, res, next) {
    db.all('SELECT token FROM sessions WHERE token=?;', [req.cookies['token']], function (err, data) {
        if (data.length > 0) {
            res.sendFile(__dirname + '/public/deconnexion.html');
        }
        else{
            res.sendFile(__dirname + '/public/index.html');
        }
    });
});
app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});
app.post("/login", function(req, res, next) {
    //vérifie si le compte est dans la base
    db.all('SELECT rowid, ident, password FROM users WHERE ident=? AND password=?;',[req.body.login,req.body.mdp], function(err, data) {
        if(err)
        {
            console.error(err);
        }
    	if(data.length>0)
		{
            var token = shajs('sha256').update(Date.now()+req.body.login+alea()).digest('hex');
            //vérifie si le compte possède un token dans la base
            db.all('SELECT token FROM sessions WHERE ident=?;',[req.body.login], function(err, data2) {
                if(err)
                {
                    console.error(err);
                }
                //si le token existe
                if(data2.length>0)
                {
                    db.all('SELECT token FROM sessions WHERE token=?;',[req.cookies['token']], function(err, data3) {
                        if (err)
                        {
                            console.error(err);
                        }
                        //si le token = cookie
                        else if (data3.length > 0)
                        {
                            //res.json({status: true, token: req.cookies});
                            console.log('cookie = token');
                            res.sendFile(__dirname + '/public/deconnexion.html');
                        }

                        //si le token != cookie on met a jour le token
                        else
                        {

                            db.all('UPDATE sessions SET token=? WHERE ident=?;', [token, req.body.login], function (err, data) {
                                res.cookie('token', token);
                                //console.log('cookie != token');
                                //res.json({status: true, token: token});
                                res.sendFile(__dirname + '/public/deconnexion.html');
                            });
                        }
                    });
                }
				else{
                    //si pas de token alors on ajoute un token dans la base
                    db.all('INSERT INTO sessions VALUES (?,?);',[req.body.login,token], function(err, data) {
                        res.cookie('token', token);
                        //console.log('pas de token');
                        //res.json({status:true,token:token});
                        res.sendFile(__dirname + '/public/deconnexion.html');
                    });
				}
            });
        }
        else{
    	    //si pas de résultat de connexion on affiche false
    		res.json({status:false});
		}
    });
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
